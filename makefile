all: BPlayer

BPlayer.o: BPlayer.h BPlayer.cpp
	g++ -c BPlayer.cpp

BPlayer: BPlayer.o BPlayerClient.cpp
	g++ -o client BPlayer.o BPlayerClient.cpp

clean:
	rm client *.o